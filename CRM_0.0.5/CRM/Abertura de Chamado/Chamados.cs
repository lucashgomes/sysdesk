﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace CRM.Abertura_de_Chamado
{
    public class Chamados
    {
        private string UsuarioAfetado = string.Empty;
        private int Departamento = 0;
        private int NumPA = 0;
        private int Titulo = 0;
        private int Urgencia = 0;
        private int Status = 0;
        private string Observacoes = string.Empty;

        private string cabecalho = string.Empty;

        SqlConnection sqlConn = null;
        string strConn = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Wanderson\Desktop\CRM_0.0.5\CRM\database\dbsysdesk.mdf;Integrated Security=True;Connect Timeout=30";

        SqlCommand cmd = null;
        DataTable dataTable = new DataTable();
        string _SQL = string.Empty;

        public Chamados(string usuarioAfetado, int departamento, int numPA, int titulo, int urgencia, string observacoes)
        {
            this.UsuarioAfetado = usuarioAfetado;
            this.Departamento = departamento;
            this.NumPA = numPA;
            this.Titulo = titulo;
            this.Urgencia = urgencia;
            this.Observacoes = observacoes;
        }

        //public void preencheLabelIdChamado(string lblCabecalho)
        //{
        //    SqlConnection sqlConn = null;
        //    sqlConn = new SqlConnection(strConn);

        //    SqlCommand cmd = null;
        //    DataTable dataTable = new DataTable();
        //    SqlDataReader datareader;

        //    this.cabecalho = lblCabecalho;

        //    sqlConn.Open();

        //    _SQL = "SELECT MAX(COD_CHAMADO) FROM TB_CHAMADO";

        //    cmd = new SqlCommand(_SQL, sqlConn);

        //    datareader = cmd.ExecuteReader();
        //    dataTable.Load(datareader);
        //    cabecalho = dataTable.ToString();

        //}

        public void adicionar()
        { 
            sqlConn = new SqlConnection(strConn);
            sqlConn.Open();

            try { 
                cmd = new SqlCommand("INSERT INTO TB_CHAMADO VALUES ( ' " + UsuarioAfetado + " ' , " 
                                                                          + Departamento + " , " 
                                                                          + NumPA + " , " 
                                                                          + Titulo + " , " 
                                                                          + Urgencia + " , "
                                                                          + Status + ", '"
                                                                          + Observacoes + "' ) ", sqlConn);
                cmd.ExecuteNonQuery();

                System.Windows.Forms.MessageBox.Show("Chamado inserido com sucesso!");;

            }catch(SqlException e)
            {
                System.Windows.Forms.MessageBox.Show("Erro ao inserir: " + e);
            }
        }

        
    }
}
