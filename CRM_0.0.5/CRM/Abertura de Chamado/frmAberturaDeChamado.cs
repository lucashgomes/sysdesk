﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using CRM.Abertura_de_Chamado;

namespace CRM.Abertura_de_Chamado
{
    public partial class AberturaChamado : Form
    {
        public AberturaChamado()
        {
            InitializeComponent();
        }

        Chamados chamados;
        frmMenu_Principal menuPrincipal = new frmMenu_Principal();

        SqlConnection sqlConn = null;
        string strConn = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Wanderson\Desktop\CRM_0.0.5\CRM\database\dbsysdesk.mdf;Integrated Security=True;Connect Timeout=30";

        
        string _SQL = string.Empty;

        private void btnAdicionar_Click(object sender, EventArgs e)
        {

            chamados = new Chamados(
                txtUsuarioAfetado.Text,
                Convert.ToInt32(cbDepartamento.SelectedIndex.ToString()),
                Convert.ToInt32(txtNumeroPA.Text),
                Convert.ToInt32(cbTitulo.SelectedIndex.ToString()),
                Convert.ToInt32(cbUrgencia.SelectedIndex.ToString()),
                txtObservacoes.Text
            );

            chamados.adicionar();
            limpaCampos();

        }

        private void btnLimpar_Click_1(object sender, EventArgs e)
        {
            limpaCampos();
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
           
        }

        private void AberturaChamado_Load(object sender, EventArgs e)
        {
            populaCbDepartamento();
            populaCbTitulo();
            populaCbUrgencia();
        }

        private void limpaCampos()
        {

            txtUsuarioAfetado.Clear();
            cbDepartamento.ResetText();
            txtNumeroPA.Clear();
            cbTitulo.ResetText();
            cbUrgencia.ResetText();
            txtObservacoes.Clear();
            txtUsuarioAfetado.Focus();
        }

        private void populaCbDepartamento()
        {

            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataReader datareader;

            sqlConn = new SqlConnection(strConn);
            sqlConn.Open();

            _SQL = "SELECT NOME_DEPARTAMENTO FROM TB_DEPARTAMENTO";

            cmd = new SqlCommand(_SQL, sqlConn);

            datareader = cmd.ExecuteReader();

            if (datareader.HasRows)
            {
                dataTable.Load(datareader);

                cbDepartamento.DisplayMember = "NOME_DEPARTAMENTO";
                cbDepartamento.DataSource = dataTable;

            }

            sqlConn.Close();
        }


        private void populaCbTitulo()
        {
            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataReader datareader;

            sqlConn = new SqlConnection(strConn);
            sqlConn.Open();

            _SQL = "SELECT TITULO FROM TB_TITULOCHAMADO";

            cmd = new SqlCommand(_SQL, sqlConn);

            datareader = cmd.ExecuteReader();

            if (datareader.HasRows)
            {
                dataTable.Load(datareader);
                cbTitulo.DisplayMember = "TITULO";
                cbTitulo.DataSource = dataTable;

   
            }
            sqlConn.Close();
        }

        private void populaCbUrgencia()
        {
            SqlCommand cmd = null;
            DataTable dataTable = new DataTable();
            SqlDataReader datareader;

            sqlConn = new SqlConnection(strConn);
            sqlConn.Open();

            _SQL = "SELECT TB_URGENCIA.TIPO_URGENCIA FROM TB_URGENCIA";

            cmd = new SqlCommand(_SQL, sqlConn);

            datareader = cmd.ExecuteReader();

            if (datareader.HasRows)
            {
                dataTable.Load(datareader);
                cbUrgencia.DisplayMember = "TIPO_URGENCIA";
                cbUrgencia.DataSource = dataTable;
            }
            _SQL = string.Empty;
            sqlConn.Close();
        }

        private void AberturaChamado_FormClosed(object sender, FormClosedEventArgs e)
        {
            Dispose();
            menuPrincipal.ShowDialog();
        }
        
    }
}

