﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CRM
{
    public class Banco
    {
        public string sqlQuery = string.Empty;
        public SqlConnection sqlConn = null;
        private string strConn = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Wanderson\Desktop\CRM_0.0.5\CRM\database\dbsysdesk.mdf;Integrated Security=True;Connect Timeout=30";

        public bool conectar()
        {
            sqlConn = new SqlConnection(strConn);

            try
            {
                sqlConn.Open();
                return true;

            }catch(SqlException e){

                System.Windows.Forms.MessageBox.Show("Erro ao se conectar ao banco de dados: " + e);
                return false;
            }
        }

        public bool desconectar()
        {
            //se a conexao não está fechada, ela é fechada. Retorna true para futuras verificações
            if (sqlConn.State != ConnectionState.Closed)
            {
                sqlConn.Close();
                return true;
            }
            else
            {
                sqlConn.Dispose(); //conexão retirada da memória
                return false;
            }
        }

        public string stringConexao()
        {
            string stringConn;
            stringConn = strConn;
            return stringConn;
        }


    
    }
}
