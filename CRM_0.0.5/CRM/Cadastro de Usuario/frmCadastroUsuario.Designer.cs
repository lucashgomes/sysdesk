﻿namespace Tela_Cadastro
{
    partial class frm_Cadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frm_Cadastro));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNome = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.maskCPF = new System.Windows.Forms.MaskedTextBox();
            this.maskRG = new System.Windows.Forms.MaskedTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.maskSenha = new System.Windows.Forms.MaskedTextBox();
            this.txtUsuario = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rbtAssistencia_Cliente = new System.Windows.Forms.RadioButton();
            this.rbtFinanceiro = new System.Windows.Forms.RadioButton();
            this.rbtBanco_Dados = new System.Windows.Forms.RadioButton();
            this.rbtServicos_Sociais = new System.Windows.Forms.RadioButton();
            this.rbtJuridico = new System.Windows.Forms.RadioButton();
            this.rbtInfraestrutura = new System.Windows.Forms.RadioButton();
            this.rbtTrafego = new System.Windows.Forms.RadioButton();
            this.rbtDesenvolvimento = new System.Windows.Forms.RadioButton();
            this.rbtRecursos_Humanos = new System.Windows.Forms.RadioButton();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nome:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 68);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "RG:";
            // 
            // txtNome
            // 
            this.txtNome.Location = new System.Drawing.Point(66, 31);
            this.txtNome.Name = "txtNome";
            this.txtNome.Size = new System.Drawing.Size(147, 20);
            this.txtNome.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.maskCPF);
            this.groupBox1.Controls.Add(this.maskRG);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txtNome);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(224, 171);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados pessoais";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.Red;
            this.label8.Location = new System.Drawing.Point(38, 97);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(12, 15);
            this.label8.TabIndex = 11;
            this.label8.Text = "*";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.Red;
            this.label7.Location = new System.Drawing.Point(38, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(12, 15);
            this.label7.TabIndex = 10;
            this.label7.Text = "*";
            // 
            // maskCPF
            // 
            this.maskCPF.Location = new System.Drawing.Point(66, 99);
            this.maskCPF.Mask = "000.000.000-00";
            this.maskCPF.Name = "maskCPF";
            this.maskCPF.Size = new System.Drawing.Size(147, 20);
            this.maskCPF.TabIndex = 4;
            // 
            // maskRG
            // 
            this.maskRG.Location = new System.Drawing.Point(66, 65);
            this.maskRG.Mask = "00.000.000-0";
            this.maskRG.Name = "maskRG";
            this.maskRG.Size = new System.Drawing.Size(147, 20);
            this.maskRG.TabIndex = 3;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 102);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "CPF:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 137);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(38, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Cargo:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.maskSenha);
            this.groupBox2.Controls.Add(this.txtUsuario);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(12, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(224, 94);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dados do usuário";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.Color.Red;
            this.label10.Location = new System.Drawing.Point(45, 63);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(12, 15);
            this.label10.TabIndex = 13;
            this.label10.Text = "*";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.Red;
            this.label9.Location = new System.Drawing.Point(46, 27);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(12, 15);
            this.label9.TabIndex = 12;
            this.label9.Text = "*";
            // 
            // maskSenha
            // 
            this.maskSenha.Location = new System.Drawing.Point(66, 65);
            this.maskSenha.Name = "maskSenha";
            this.maskSenha.PasswordChar = '*';
            this.maskSenha.Size = new System.Drawing.Size(147, 20);
            this.maskSenha.TabIndex = 7;
            this.maskSenha.UseSystemPasswordChar = true;
            // 
            // txtUsuario
            // 
            this.txtUsuario.Location = new System.Drawing.Point(66, 29);
            this.txtUsuario.Name = "txtUsuario";
            this.txtUsuario.Size = new System.Drawing.Size(147, 20);
            this.txtUsuario.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 68);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Senha:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Usuário:";
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionar.Image")));
            this.btnAdicionar.Location = new System.Drawing.Point(21, 294);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(50, 45);
            this.btnAdicionar.TabIndex = 0;
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // btnCancelar
            // 
            this.btnCancelar.Image = ((System.Drawing.Image)(resources.GetObject("btnCancelar.Image")));
            this.btnCancelar.Location = new System.Drawing.Point(331, 294);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(50, 45);
            this.btnCancelar.TabIndex = 1;
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnCancelar_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rbtAssistencia_Cliente);
            this.groupBox3.Controls.Add(this.rbtFinanceiro);
            this.groupBox3.Controls.Add(this.rbtBanco_Dados);
            this.groupBox3.Controls.Add(this.rbtServicos_Sociais);
            this.groupBox3.Controls.Add(this.rbtJuridico);
            this.groupBox3.Controls.Add(this.rbtInfraestrutura);
            this.groupBox3.Controls.Add(this.rbtTrafego);
            this.groupBox3.Controls.Add(this.rbtDesenvolvimento);
            this.groupBox3.Controls.Add(this.rbtRecursos_Humanos);
            this.groupBox3.Location = new System.Drawing.Point(257, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(200, 271);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Setor";
            // 
            // rbtAssistencia_Cliente
            // 
            this.rbtAssistencia_Cliente.AutoSize = true;
            this.rbtAssistencia_Cliente.Location = new System.Drawing.Point(6, 187);
            this.rbtAssistencia_Cliente.Name = "rbtAssistencia_Cliente";
            this.rbtAssistencia_Cliente.Size = new System.Drawing.Size(104, 17);
            this.rbtAssistencia_Cliente.TabIndex = 8;
            this.rbtAssistencia_Cliente.TabStop = true;
            this.rbtAssistencia_Cliente.Text = "Suporte Técnico";
            this.rbtAssistencia_Cliente.UseVisualStyleBackColor = true;
            // 
            // rbtFinanceiro
            // 
            this.rbtFinanceiro.AutoSize = true;
            this.rbtFinanceiro.Location = new System.Drawing.Point(6, 72);
            this.rbtFinanceiro.Name = "rbtFinanceiro";
            this.rbtFinanceiro.Size = new System.Drawing.Size(74, 17);
            this.rbtFinanceiro.TabIndex = 11;
            this.rbtFinanceiro.TabStop = true;
            this.rbtFinanceiro.Text = "Financeiro";
            this.rbtFinanceiro.UseVisualStyleBackColor = true;
            // 
            // rbtBanco_Dados
            // 
            this.rbtBanco_Dados.AutoSize = true;
            this.rbtBanco_Dados.Location = new System.Drawing.Point(6, 27);
            this.rbtBanco_Dados.Name = "rbtBanco_Dados";
            this.rbtBanco_Dados.Size = new System.Drawing.Size(105, 17);
            this.rbtBanco_Dados.TabIndex = 9;
            this.rbtBanco_Dados.TabStop = true;
            this.rbtBanco_Dados.Text = "Banco de Dados";
            this.rbtBanco_Dados.UseVisualStyleBackColor = true;
            // 
            // rbtServicos_Sociais
            // 
            this.rbtServicos_Sociais.AutoSize = true;
            this.rbtServicos_Sociais.Location = new System.Drawing.Point(6, 164);
            this.rbtServicos_Sociais.Name = "rbtServicos_Sociais";
            this.rbtServicos_Sociais.Size = new System.Drawing.Size(103, 17);
            this.rbtServicos_Sociais.TabIndex = 15;
            this.rbtServicos_Sociais.TabStop = true;
            this.rbtServicos_Sociais.Text = "Serviços Sociais";
            this.rbtServicos_Sociais.UseVisualStyleBackColor = true;
            // 
            // rbtJuridico
            // 
            this.rbtJuridico.AutoSize = true;
            this.rbtJuridico.Location = new System.Drawing.Point(6, 118);
            this.rbtJuridico.Name = "rbtJuridico";
            this.rbtJuridico.Size = new System.Drawing.Size(118, 17);
            this.rbtJuridico.TabIndex = 13;
            this.rbtJuridico.TabStop = true;
            this.rbtJuridico.Text = "Processos Juridicos";
            this.rbtJuridico.UseVisualStyleBackColor = true;
            // 
            // rbtInfraestrutura
            // 
            this.rbtInfraestrutura.AutoSize = true;
            this.rbtInfraestrutura.Location = new System.Drawing.Point(6, 95);
            this.rbtInfraestrutura.Name = "rbtInfraestrutura";
            this.rbtInfraestrutura.Size = new System.Drawing.Size(122, 17);
            this.rbtInfraestrutura.TabIndex = 12;
            this.rbtInfraestrutura.TabStop = true;
            this.rbtInfraestrutura.Text = "Infraestrutura Predial";
            this.rbtInfraestrutura.UseVisualStyleBackColor = true;
            // 
            // rbtTrafego
            // 
            this.rbtTrafego.AutoSize = true;
            this.rbtTrafego.Location = new System.Drawing.Point(6, 210);
            this.rbtTrafego.Name = "rbtTrafego";
            this.rbtTrafego.Size = new System.Drawing.Size(62, 17);
            this.rbtTrafego.TabIndex = 16;
            this.rbtTrafego.TabStop = true;
            this.rbtTrafego.Text = "Tráfego";
            this.rbtTrafego.UseVisualStyleBackColor = true;
            // 
            // rbtDesenvolvimento
            // 
            this.rbtDesenvolvimento.AutoSize = true;
            this.rbtDesenvolvimento.Location = new System.Drawing.Point(6, 49);
            this.rbtDesenvolvimento.Name = "rbtDesenvolvimento";
            this.rbtDesenvolvimento.Size = new System.Drawing.Size(107, 17);
            this.rbtDesenvolvimento.TabIndex = 10;
            this.rbtDesenvolvimento.TabStop = true;
            this.rbtDesenvolvimento.Text = "Desenvolvimento";
            this.rbtDesenvolvimento.UseVisualStyleBackColor = true;
            // 
            // rbtRecursos_Humanos
            // 
            this.rbtRecursos_Humanos.AutoSize = true;
            this.rbtRecursos_Humanos.Location = new System.Drawing.Point(6, 141);
            this.rbtRecursos_Humanos.Name = "rbtRecursos_Humanos";
            this.rbtRecursos_Humanos.Size = new System.Drawing.Size(118, 17);
            this.rbtRecursos_Humanos.TabIndex = 14;
            this.rbtRecursos_Humanos.TabStop = true;
            this.rbtRecursos_Humanos.Text = "Recursos Humanos";
            this.rbtRecursos_Humanos.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Estagiário",
            "Auxiliar",
            "Técnico",
            "Analista",
            "Consultor",
            "Gerente",
            "Diretor"});
            this.comboBox1.Location = new System.Drawing.Point(66, 129);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(147, 21);
            this.comboBox1.TabIndex = 12;
            // 
            // frm_Cadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 351);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "frm_Cadastro";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Registro de usuário";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtNome;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.MaskedTextBox maskCPF;
        private System.Windows.Forms.MaskedTextBox maskRG;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.MaskedTextBox maskSenha;
        private System.Windows.Forms.TextBox txtUsuario;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rbtAssistencia_Cliente;
        private System.Windows.Forms.RadioButton rbtFinanceiro;
        private System.Windows.Forms.RadioButton rbtBanco_Dados;
        private System.Windows.Forms.RadioButton rbtServicos_Sociais;
        private System.Windows.Forms.RadioButton rbtJuridico;
        private System.Windows.Forms.RadioButton rbtInfraestrutura;
        private System.Windows.Forms.RadioButton rbtTrafego;
        private System.Windows.Forms.RadioButton rbtDesenvolvimento;
        private System.Windows.Forms.RadioButton rbtRecursos_Humanos;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox1;
    }
}

