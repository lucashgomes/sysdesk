﻿namespace CRM.Fila_de_Chamados
{
    partial class frmFilaDeChamados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grvFilaDeChamados = new System.Windows.Forms.DataGridView();
            this.btnProcessando = new System.Windows.Forms.Button();
            this.btnPendente = new System.Windows.Forms.Button();
            this.btnSolucionado = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBuscar = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.grvFilaDeChamados)).BeginInit();
            this.SuspendLayout();
            // 
            // grvFilaDeChamados
            // 
            this.grvFilaDeChamados.AllowUserToAddRows = false;
            this.grvFilaDeChamados.AllowUserToDeleteRows = false;
            this.grvFilaDeChamados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.grvFilaDeChamados.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.grvFilaDeChamados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.grvFilaDeChamados.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised;
            this.grvFilaDeChamados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvFilaDeChamados.Location = new System.Drawing.Point(12, 50);
            this.grvFilaDeChamados.Name = "grvFilaDeChamados";
            this.grvFilaDeChamados.ReadOnly = true;
            this.grvFilaDeChamados.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.grvFilaDeChamados.Size = new System.Drawing.Size(808, 447);
            this.grvFilaDeChamados.TabIndex = 0;
            // 
            // btnProcessando
            // 
            this.btnProcessando.Location = new System.Drawing.Point(456, 12);
            this.btnProcessando.Name = "btnProcessando";
            this.btnProcessando.Size = new System.Drawing.Size(117, 32);
            this.btnProcessando.TabIndex = 1;
            this.btnProcessando.Text = "Processando";
            this.btnProcessando.UseVisualStyleBackColor = true;
            this.btnProcessando.Click += new System.EventHandler(this.btnProcessando_Click);
            // 
            // btnPendente
            // 
            this.btnPendente.Location = new System.Drawing.Point(579, 12);
            this.btnPendente.Name = "btnPendente";
            this.btnPendente.Size = new System.Drawing.Size(117, 32);
            this.btnPendente.TabIndex = 2;
            this.btnPendente.Text = "Pendente";
            this.btnPendente.UseVisualStyleBackColor = true;
            this.btnPendente.Click += new System.EventHandler(this.btnPendente_Click);
            // 
            // btnSolucionado
            // 
            this.btnSolucionado.Location = new System.Drawing.Point(702, 12);
            this.btnSolucionado.Name = "btnSolucionado";
            this.btnSolucionado.Size = new System.Drawing.Size(117, 32);
            this.btnSolucionado.TabIndex = 3;
            this.btnSolucionado.Text = "Solucionado";
            this.btnSolucionado.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Buscar por código:";
            // 
            // txtBuscar
            // 
            this.txtBuscar.Location = new System.Drawing.Point(114, 24);
            this.txtBuscar.Name = "txtBuscar";
            this.txtBuscar.Size = new System.Drawing.Size(196, 20);
            this.txtBuscar.TabIndex = 5;
            // 
            // frmFilaDeChamados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(832, 509);
            this.Controls.Add(this.txtBuscar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnSolucionado);
            this.Controls.Add(this.btnPendente);
            this.Controls.Add(this.btnProcessando);
            this.Controls.Add(this.grvFilaDeChamados);
            this.MaximizeBox = false;
            this.Name = "frmFilaDeChamados";
            this.Text = "Fila de Chamados";
            this.Load += new System.EventHandler(this.frmFilaDeChamados_Load);
            ((System.ComponentModel.ISupportInitialize)(this.grvFilaDeChamados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView grvFilaDeChamados;
        private System.Windows.Forms.Button btnProcessando;
        private System.Windows.Forms.Button btnPendente;
        private System.Windows.Forms.Button btnSolucionado;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBuscar;
    }
}