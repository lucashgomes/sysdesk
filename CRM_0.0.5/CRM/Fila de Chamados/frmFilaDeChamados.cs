﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRM.Fila_de_Chamados
{
    public partial class frmFilaDeChamados : Form
    {
        public frmFilaDeChamados()
        {
            InitializeComponent();
        }

        Banco banco = new Banco();
        
        SqlCommand cmd;

        private  void atualizaForm()
        {
            Refresh(); 
        }

        private void frmFilaDeChamados_Load(object sender, EventArgs e)
        {
            string _sql = "SELECT TB_CHAMADO.COD_CHAMADO AS 'Codigo', " +
                                 "TB_TITULOCHAMADO.TITULO AS 'Titulo', " +
                              // "TB_USUARIO.NOME AS 'Requerente'," +
                                 "TB_DEPARTAMENTO.NOME_DEPARTAMENTO AS 'Atribuido a', " +
                                 "TB_URGENCIA.TIPO_URGENCIA AS 'Prioridade', " +
                                 "TB_STATUS.DESCRICAO AS 'Status', " +
                                 "TB_CHAMADO.OBSERVACOES AS 'Observações'" +
                                 "FROM TB_CHAMADO INNER JOIN TB_TITULOCHAMADO ON TB_CHAMADO.COD_TITULO = TB_TITULOCHAMADO.COD_TITULO " +
                            //   "INNER JOIN TB_CHAMADO.COD_"
                                 "INNER JOIN TB_DEPARTAMENTO ON TB_CHAMADO.COD_DEPARTAMENTO = TB_DEPARTAMENTO.COD_DEPARTAMENTO " +
                                 "INNER JOIN TB_URGENCIA ON TB_CHAMADO.COD_URGENCIA = TB_URGENCIA.COD_URGENCIA " + 
                                 "INNER JOIN TB_STATUS ON TB_CHAMADO.COD_STATUS = TB_STATUS.COD_STATUS " +
                                 "ORDER BY TB_CHAMADO.COD_CHAMADO DESC";
            
            DataTable dt = new DataTable();

            if (banco.conectar())
            {
                cmd = new SqlCommand(_sql, banco.sqlConn);
                cmd.CommandType = CommandType.Text;

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);

                grvFilaDeChamados.DataSource = dt;

                int colunas = dt.Columns.Count;

                //Realiza a ordenação do Data Grid
                for (int i = 0; i < colunas; i++)
                {
                    grvFilaDeChamados.Sort(grvFilaDeChamados.Columns[i], ListSortDirection.Ascending);
                }
            }
            
            
        }

        private void btnProcessando_Click(object sender, EventArgs e)
        {

            if(grvFilaDeChamados.CurrentRow.Cells[4].Value.ToString() == "Processando")
            {
                MessageBox.Show("O chamado já possui o status de PROCESSANDO.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (DialogResult.Yes == MessageBox.Show("Deseja alterar o status deste chamado para PROCESSANDO?", "Confirmação de alteração de status", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                string v = grvFilaDeChamados.CurrentRow.Cells[0].Value.ToString();
                int valorCelula = 0;

                if (int.TryParse(v, out valorCelula))
                {
                    MessageBox.Show("OK");
                }
                
                //if (banco.conectar())
                //{
                //    try
                //    {

                //        cmd = new SqlCommand("UPDATE TB_CHAMADO SET COD_STATUS = 0 WHERE COD_CHAMADO = " + Int16.Parse(grvFilaDeChamados.CurrentRow.Cells[0].Value.ToString()) + " )", banco.sqlConn);
                //        cmd.ExecuteNonQuery();

                //        MessageBox.Show("Status alterado com sucesso", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                //    }
                //    catch (SqlException ex)
                //    {
                //        System.Windows.Forms.MessageBox.Show("Erro ao inserir: " + ex);
                //    }
                //}
            }
        }

        private void btnPendente_Click(object sender, EventArgs e)
        {
            if (grvFilaDeChamados.CurrentRow.Cells[4].Value.ToString() == "Pendente")
            {
                MessageBox.Show("O chamado já se encontra pendente.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            if (DialogResult.Yes == MessageBox.Show("Deseja alterar o status deste chamado para PENDENTE?", "Confirmação de alteração de status", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2))
            {
                if (banco.conectar())
                {
                    try
                    {
                        cmd = new SqlCommand("UPDATE TB_CHAMADO SET COD_STATUS = 1 WHERE COD_CHAMADO = " + Convert.ToInt32(grvFilaDeChamados.CurrentRow.Cells[0].Value.ToString()) + " )", banco.sqlConn);
                        cmd.ExecuteNonQuery();

                        MessageBox.Show("Status alterado com sucesso", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        atualizaForm();
                    }
                    catch (SqlException ex)
                    {
                        System.Windows.Forms.MessageBox.Show("Erro ao inserir: " + ex);
                    }
                }
            }
        }
    }
}
