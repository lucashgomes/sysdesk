﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CRM
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();

        }

        public bool conectado = false;
        private string _Sql = string.Empty;

        Banco banco;
        SqlCommand cmd;

        public void logar()
        {
            banco = new Banco();
            string user, password;

                try
                {
                    //se a conexão com o banco é bem sucedida
                    if (banco.conectar())
                    {
                        user = txtUsuario.Text;
                        password = maskSenha.Text;

                        _Sql = "SELECT COUNT(ID_USUARIO) FROM TB_LOGIN WHERE ID_USUARIO = @usuario AND SENHA = @senha";

                        cmd = new SqlCommand(_Sql, banco.sqlConn);

                        cmd.Parameters.Add("@usuario", SqlDbType.VarChar).Value = user;
                        cmd.Parameters.Add("@senha", SqlDbType.VarChar).Value = password;

                        int verifica_logon = (int)cmd.ExecuteScalar();

                        //se o user/senha estão corretos, retira o frmLogin da memória e atribui conetado=true para a Program.cs
                        if (verifica_logon > 0)
                        {
                            this.Dispose();
                            conectado = true;
                        }

                        //senão exibe mensagem de erro, limpa os campos user/senha e foca em user
                        else
                        {
                            MessageBox.Show("Usuário ou senha inválidos!");

                            txtUsuario.Clear();
                            maskSenha.Clear();
                            txtUsuario.Focus();

                            conectado = false;
                        }
                    }
                }
                catch (SqlException erro)
                {
                    MessageBox.Show(("Falha ao conectar ao servidor de banco de dados.") + erro);
                }

            banco.desconectar();
        }


        private void btnConfirmar_Click(object sender, EventArgs e)
        {

            logar();

        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void maskSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                logar();
            }
        }

        private void frmLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }
        }

        private void txtUsuario_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Application.Exit();
            }
        }
    }
}
