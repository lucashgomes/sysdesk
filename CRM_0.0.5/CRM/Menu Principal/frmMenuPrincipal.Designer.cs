﻿namespace CRM
{
    partial class frmMenu_Principal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.btnAbrirChamado = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.btnNovo_Usuario = new System.Windows.Forms.Button();
            this.btnAdicionar_Chamado = new System.Windows.Forms.Button();
            this.tabChamados = new System.Windows.Forms.TabControl();
            this.tabMeus_Chamados = new System.Windows.Forms.TabPage();
            this.grvMeus_Chamados = new System.Windows.Forms.DataGridView();
            this.colId_Chamados = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colTitulo_Chamado = new System.Windows.Forms.DataGridViewButtonColumn();
            this.colData_Abertura = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colRequerente = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colAtribuido = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colPrioridade = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colSLA = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabProcessando = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn1 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grvProcessando = new System.Windows.Forms.DataGridView();
            this.tabPendentes = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn2 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn11 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn12 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn13 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn14 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grvPendentes = new System.Windows.Forms.DataGridView();
            this.tabSolucionados = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn15 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewButtonColumn3 = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dataGridViewTextBoxColumn16 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn17 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn18 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn19 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn20 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn21 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grvSolucionados = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.tabChamados.SuspendLayout();
            this.tabMeus_Chamados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grvMeus_Chamados)).BeginInit();
            this.tabProcessando.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProcessando)).BeginInit();
            this.tabPendentes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPendentes)).BeginInit();
            this.tabSolucionados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSolucionados)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.BurlyWood;
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.btnAbrirChamado);
            this.panel1.Controls.Add(this.btnSair);
            this.panel1.Controls.Add(this.btnNovo_Usuario);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1058, 59);
            this.panel1.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(260, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(109, 53);
            this.button1.TabIndex = 5;
            this.button1.Text = "Fila de Chamados";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAbrirChamado
            // 
            this.btnAbrirChamado.Location = new System.Drawing.Point(136, 3);
            this.btnAbrirChamado.Name = "btnAbrirChamado";
            this.btnAbrirChamado.Size = new System.Drawing.Size(109, 53);
            this.btnAbrirChamado.TabIndex = 4;
            this.btnAbrirChamado.Text = "Abrir Chamado";
            this.btnAbrirChamado.UseVisualStyleBackColor = true;
            this.btnAbrirChamado.Click += new System.EventHandler(this.btnAbrirChamado_Click);
            // 
            // btnSair
            // 
            this.btnSair.Location = new System.Drawing.Point(385, 3);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(85, 53);
            this.btnSair.TabIndex = 3;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            // 
            // btnNovo_Usuario
            // 
            this.btnNovo_Usuario.Location = new System.Drawing.Point(12, 3);
            this.btnNovo_Usuario.Name = "btnNovo_Usuario";
            this.btnNovo_Usuario.Size = new System.Drawing.Size(109, 53);
            this.btnNovo_Usuario.TabIndex = 0;
            this.btnNovo_Usuario.Text = "Novo Usuário";
            this.btnNovo_Usuario.UseVisualStyleBackColor = true;
            this.btnNovo_Usuario.Click += new System.EventHandler(this.btnNovo_Usuario_Click);
            // 
            // btnAdicionar_Chamado
            // 
            this.btnAdicionar_Chamado.Location = new System.Drawing.Point(24, 86);
            this.btnAdicionar_Chamado.Name = "btnAdicionar_Chamado";
            this.btnAdicionar_Chamado.Size = new System.Drawing.Size(39, 29);
            this.btnAdicionar_Chamado.TabIndex = 1;
            this.btnAdicionar_Chamado.Text = "+";
            this.btnAdicionar_Chamado.UseVisualStyleBackColor = true;
            // 
            // tabChamados
            // 
            this.tabChamados.Controls.Add(this.tabMeus_Chamados);
            this.tabChamados.Controls.Add(this.tabProcessando);
            this.tabChamados.Controls.Add(this.tabPendentes);
            this.tabChamados.Controls.Add(this.tabSolucionados);
            this.tabChamados.ItemSize = new System.Drawing.Size(120, 25);
            this.tabChamados.Location = new System.Drawing.Point(24, 121);
            this.tabChamados.Name = "tabChamados";
            this.tabChamados.SelectedIndex = 0;
            this.tabChamados.Size = new System.Drawing.Size(1046, 456);
            this.tabChamados.TabIndex = 1;
            // 
            // tabMeus_Chamados
            // 
            this.tabMeus_Chamados.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabMeus_Chamados.Controls.Add(this.grvMeus_Chamados);
            this.tabMeus_Chamados.Location = new System.Drawing.Point(4, 29);
            this.tabMeus_Chamados.Name = "tabMeus_Chamados";
            this.tabMeus_Chamados.Padding = new System.Windows.Forms.Padding(3);
            this.tabMeus_Chamados.Size = new System.Drawing.Size(1038, 423);
            this.tabMeus_Chamados.TabIndex = 0;
            this.tabMeus_Chamados.Text = "Meus Chamados";
            this.tabMeus_Chamados.UseVisualStyleBackColor = true;
            // 
            // grvMeus_Chamados
            // 
            this.grvMeus_Chamados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvMeus_Chamados.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colId_Chamados,
            this.colTitulo_Chamado,
            this.colData_Abertura,
            this.colRequerente,
            this.colAtribuido,
            this.colPrioridade,
            this.colSLA,
            this.colStatus});
            this.grvMeus_Chamados.GridColor = System.Drawing.Color.Maroon;
            this.grvMeus_Chamados.Location = new System.Drawing.Point(0, 0);
            this.grvMeus_Chamados.Name = "grvMeus_Chamados";
            this.grvMeus_Chamados.Size = new System.Drawing.Size(1038, 430);
            this.grvMeus_Chamados.TabIndex = 0;
            // 
            // colId_Chamados
            // 
            this.colId_Chamados.Frozen = true;
            this.colId_Chamados.HeaderText = "ID";
            this.colId_Chamados.Name = "colId_Chamados";
            // 
            // colTitulo_Chamado
            // 
            this.colTitulo_Chamado.Frozen = true;
            this.colTitulo_Chamado.HeaderText = "Título";
            this.colTitulo_Chamado.Name = "colTitulo_Chamado";
            // 
            // colData_Abertura
            // 
            this.colData_Abertura.FillWeight = 150F;
            this.colData_Abertura.Frozen = true;
            this.colData_Abertura.HeaderText = "Data de Abertura";
            this.colData_Abertura.MinimumWidth = 15;
            this.colData_Abertura.Name = "colData_Abertura";
            this.colData_Abertura.Width = 150;
            // 
            // colRequerente
            // 
            this.colRequerente.HeaderText = "Requerente";
            this.colRequerente.Name = "colRequerente";
            // 
            // colAtribuido
            // 
            this.colAtribuido.HeaderText = "Atribuido a";
            this.colAtribuido.Name = "colAtribuido";
            // 
            // colPrioridade
            // 
            this.colPrioridade.HeaderText = "Prioridade";
            this.colPrioridade.Name = "colPrioridade";
            // 
            // colSLA
            // 
            this.colSLA.HeaderText = "SLA";
            this.colSLA.Name = "colSLA";
            // 
            // colStatus
            // 
            this.colStatus.HeaderText = "Status";
            this.colStatus.Name = "colStatus";
            // 
            // tabProcessando
            // 
            this.tabProcessando.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.tabProcessando.Controls.Add(this.dataGridView1);
            this.tabProcessando.Controls.Add(this.grvProcessando);
            this.tabProcessando.Location = new System.Drawing.Point(4, 29);
            this.tabProcessando.Name = "tabProcessando";
            this.tabProcessando.Padding = new System.Windows.Forms.Padding(3);
            this.tabProcessando.Size = new System.Drawing.Size(1038, 423);
            this.tabProcessando.TabIndex = 1;
            this.tabProcessando.Text = "Processando";
            this.tabProcessando.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(196)))), ((int)(((byte)(196)))));
            this.dataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.RaisedHorizontal;
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewButtonColumn1,
            this.dataGridViewTextBoxColumn2,
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4,
            this.dataGridViewTextBoxColumn5,
            this.dataGridViewTextBoxColumn6,
            this.dataGridViewTextBoxColumn7});
            this.dataGridView1.GridColor = System.Drawing.Color.Maroon;
            this.dataGridView1.Location = new System.Drawing.Point(-2, -6);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1038, 430);
            this.dataGridView1.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.Frozen = true;
            this.dataGridViewTextBoxColumn1.HeaderText = "ID";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            this.dataGridViewTextBoxColumn1.Width = 43;
            // 
            // dataGridViewButtonColumn1
            // 
            this.dataGridViewButtonColumn1.Frozen = true;
            this.dataGridViewButtonColumn1.HeaderText = "Título";
            this.dataGridViewButtonColumn1.Name = "dataGridViewButtonColumn1";
            this.dataGridViewButtonColumn1.Width = 41;
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.FillWeight = 150F;
            this.dataGridViewTextBoxColumn2.Frozen = true;
            this.dataGridViewTextBoxColumn2.HeaderText = "Data de Abertura";
            this.dataGridViewTextBoxColumn2.MinimumWidth = 15;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            this.dataGridViewTextBoxColumn2.Width = 104;
            // 
            // dataGridViewTextBoxColumn3
            // 
            this.dataGridViewTextBoxColumn3.HeaderText = "Requerente";
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            this.dataGridViewTextBoxColumn3.Width = 88;
            // 
            // dataGridViewTextBoxColumn4
            // 
            this.dataGridViewTextBoxColumn4.HeaderText = "Atribuido a";
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            this.dataGridViewTextBoxColumn4.Width = 76;
            // 
            // dataGridViewTextBoxColumn5
            // 
            this.dataGridViewTextBoxColumn5.HeaderText = "Prioridade";
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            this.dataGridViewTextBoxColumn5.Width = 79;
            // 
            // dataGridViewTextBoxColumn6
            // 
            this.dataGridViewTextBoxColumn6.HeaderText = "SLA";
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            this.dataGridViewTextBoxColumn6.Width = 52;
            // 
            // dataGridViewTextBoxColumn7
            // 
            this.dataGridViewTextBoxColumn7.HeaderText = "Status";
            this.dataGridViewTextBoxColumn7.Name = "dataGridViewTextBoxColumn7";
            this.dataGridViewTextBoxColumn7.Width = 62;
            // 
            // grvProcessando
            // 
            this.grvProcessando.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvProcessando.Location = new System.Drawing.Point(0, 0);
            this.grvProcessando.Name = "grvProcessando";
            this.grvProcessando.Size = new System.Drawing.Size(1038, 434);
            this.grvProcessando.TabIndex = 0;
            // 
            // tabPendentes
            // 
            this.tabPendentes.Controls.Add(this.dataGridView2);
            this.tabPendentes.Controls.Add(this.grvPendentes);
            this.tabPendentes.Location = new System.Drawing.Point(4, 29);
            this.tabPendentes.Name = "tabPendentes";
            this.tabPendentes.Size = new System.Drawing.Size(1038, 423);
            this.tabPendentes.TabIndex = 2;
            this.tabPendentes.Text = "Pendentes";
            this.tabPendentes.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn8,
            this.dataGridViewButtonColumn2,
            this.dataGridViewTextBoxColumn9,
            this.dataGridViewTextBoxColumn10,
            this.dataGridViewTextBoxColumn11,
            this.dataGridViewTextBoxColumn12,
            this.dataGridViewTextBoxColumn13,
            this.dataGridViewTextBoxColumn14});
            this.dataGridView2.GridColor = System.Drawing.Color.Maroon;
            this.dataGridView2.Location = new System.Drawing.Point(0, -4);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(1038, 430);
            this.dataGridView2.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn8
            // 
            this.dataGridViewTextBoxColumn8.Frozen = true;
            this.dataGridViewTextBoxColumn8.HeaderText = "ID";
            this.dataGridViewTextBoxColumn8.Name = "dataGridViewTextBoxColumn8";
            // 
            // dataGridViewButtonColumn2
            // 
            this.dataGridViewButtonColumn2.Frozen = true;
            this.dataGridViewButtonColumn2.HeaderText = "Título";
            this.dataGridViewButtonColumn2.Name = "dataGridViewButtonColumn2";
            // 
            // dataGridViewTextBoxColumn9
            // 
            this.dataGridViewTextBoxColumn9.FillWeight = 150F;
            this.dataGridViewTextBoxColumn9.Frozen = true;
            this.dataGridViewTextBoxColumn9.HeaderText = "Data de Abertura";
            this.dataGridViewTextBoxColumn9.MinimumWidth = 15;
            this.dataGridViewTextBoxColumn9.Name = "dataGridViewTextBoxColumn9";
            this.dataGridViewTextBoxColumn9.Width = 150;
            // 
            // dataGridViewTextBoxColumn10
            // 
            this.dataGridViewTextBoxColumn10.HeaderText = "Requerente";
            this.dataGridViewTextBoxColumn10.Name = "dataGridViewTextBoxColumn10";
            // 
            // dataGridViewTextBoxColumn11
            // 
            this.dataGridViewTextBoxColumn11.HeaderText = "Atribuido a";
            this.dataGridViewTextBoxColumn11.Name = "dataGridViewTextBoxColumn11";
            // 
            // dataGridViewTextBoxColumn12
            // 
            this.dataGridViewTextBoxColumn12.HeaderText = "Prioridade";
            this.dataGridViewTextBoxColumn12.Name = "dataGridViewTextBoxColumn12";
            // 
            // dataGridViewTextBoxColumn13
            // 
            this.dataGridViewTextBoxColumn13.HeaderText = "SLA";
            this.dataGridViewTextBoxColumn13.Name = "dataGridViewTextBoxColumn13";
            // 
            // dataGridViewTextBoxColumn14
            // 
            this.dataGridViewTextBoxColumn14.HeaderText = "Status";
            this.dataGridViewTextBoxColumn14.Name = "dataGridViewTextBoxColumn14";
            // 
            // grvPendentes
            // 
            this.grvPendentes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvPendentes.Location = new System.Drawing.Point(0, 0);
            this.grvPendentes.Name = "grvPendentes";
            this.grvPendentes.Size = new System.Drawing.Size(1038, 434);
            this.grvPendentes.TabIndex = 0;
            // 
            // tabSolucionados
            // 
            this.tabSolucionados.Controls.Add(this.dataGridView3);
            this.tabSolucionados.Controls.Add(this.grvSolucionados);
            this.tabSolucionados.Location = new System.Drawing.Point(4, 29);
            this.tabSolucionados.Name = "tabSolucionados";
            this.tabSolucionados.Size = new System.Drawing.Size(1038, 423);
            this.tabSolucionados.TabIndex = 3;
            this.tabSolucionados.Text = "Solucionados";
            this.tabSolucionados.UseVisualStyleBackColor = true;
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn15,
            this.dataGridViewButtonColumn3,
            this.dataGridViewTextBoxColumn16,
            this.dataGridViewTextBoxColumn17,
            this.dataGridViewTextBoxColumn18,
            this.dataGridViewTextBoxColumn19,
            this.dataGridViewTextBoxColumn20,
            this.dataGridViewTextBoxColumn21});
            this.dataGridView3.GridColor = System.Drawing.Color.Maroon;
            this.dataGridView3.Location = new System.Drawing.Point(0, -4);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(1038, 430);
            this.dataGridView3.TabIndex = 1;
            // 
            // dataGridViewTextBoxColumn15
            // 
            this.dataGridViewTextBoxColumn15.Frozen = true;
            this.dataGridViewTextBoxColumn15.HeaderText = "ID";
            this.dataGridViewTextBoxColumn15.Name = "dataGridViewTextBoxColumn15";
            // 
            // dataGridViewButtonColumn3
            // 
            this.dataGridViewButtonColumn3.Frozen = true;
            this.dataGridViewButtonColumn3.HeaderText = "Título";
            this.dataGridViewButtonColumn3.Name = "dataGridViewButtonColumn3";
            // 
            // dataGridViewTextBoxColumn16
            // 
            this.dataGridViewTextBoxColumn16.FillWeight = 150F;
            this.dataGridViewTextBoxColumn16.Frozen = true;
            this.dataGridViewTextBoxColumn16.HeaderText = "Data de Abertura";
            this.dataGridViewTextBoxColumn16.MinimumWidth = 15;
            this.dataGridViewTextBoxColumn16.Name = "dataGridViewTextBoxColumn16";
            this.dataGridViewTextBoxColumn16.Width = 150;
            // 
            // dataGridViewTextBoxColumn17
            // 
            this.dataGridViewTextBoxColumn17.HeaderText = "Requerente";
            this.dataGridViewTextBoxColumn17.Name = "dataGridViewTextBoxColumn17";
            // 
            // dataGridViewTextBoxColumn18
            // 
            this.dataGridViewTextBoxColumn18.HeaderText = "Atribuido a";
            this.dataGridViewTextBoxColumn18.Name = "dataGridViewTextBoxColumn18";
            // 
            // dataGridViewTextBoxColumn19
            // 
            this.dataGridViewTextBoxColumn19.HeaderText = "Prioridade";
            this.dataGridViewTextBoxColumn19.Name = "dataGridViewTextBoxColumn19";
            // 
            // dataGridViewTextBoxColumn20
            // 
            this.dataGridViewTextBoxColumn20.HeaderText = "SLA";
            this.dataGridViewTextBoxColumn20.Name = "dataGridViewTextBoxColumn20";
            // 
            // dataGridViewTextBoxColumn21
            // 
            this.dataGridViewTextBoxColumn21.HeaderText = "Status";
            this.dataGridViewTextBoxColumn21.Name = "dataGridViewTextBoxColumn21";
            // 
            // grvSolucionados
            // 
            this.grvSolucionados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grvSolucionados.Location = new System.Drawing.Point(0, 0);
            this.grvSolucionados.Name = "grvSolucionados";
            this.grvSolucionados.Size = new System.Drawing.Size(1038, 434);
            this.grvSolucionados.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(722, 108);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Buscar:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(771, 101);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(239, 20);
            this.textBox1.TabIndex = 3;
            // 
            // frmMenu_Principal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1082, 592);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabChamados);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAdicionar_Chamado);
            this.Name = "frmMenu_Principal";
            this.Text = "Menu Principal";
            this.panel1.ResumeLayout(false);
            this.tabChamados.ResumeLayout(false);
            this.tabMeus_Chamados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grvMeus_Chamados)).EndInit();
            this.tabProcessando.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvProcessando)).EndInit();
            this.tabPendentes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvPendentes)).EndInit();
            this.tabSolucionados.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.grvSolucionados)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.Button btnNovo_Usuario;
        private System.Windows.Forms.Button btnAdicionar_Chamado;
        private System.Windows.Forms.TabControl tabChamados;
        private System.Windows.Forms.TabPage tabMeus_Chamados;
        private System.Windows.Forms.DataGridView grvMeus_Chamados;
        private System.Windows.Forms.TabPage tabProcessando;
        private System.Windows.Forms.DataGridView grvProcessando;
        private System.Windows.Forms.TabPage tabPendentes;
        private System.Windows.Forms.DataGridView grvPendentes;
        private System.Windows.Forms.TabPage tabSolucionados;
        private System.Windows.Forms.DataGridView grvSolucionados;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colId_Chamados;
        private System.Windows.Forms.DataGridViewButtonColumn colTitulo_Chamado;
        private System.Windows.Forms.DataGridViewTextBoxColumn colData_Abertura;
        private System.Windows.Forms.DataGridViewTextBoxColumn colRequerente;
        private System.Windows.Forms.DataGridViewTextBoxColumn colAtribuido;
        private System.Windows.Forms.DataGridViewTextBoxColumn colPrioridade;
        private System.Windows.Forms.DataGridViewTextBoxColumn colSLA;
        private System.Windows.Forms.DataGridViewTextBoxColumn colStatus;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn7;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn8;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn2;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn9;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn10;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn11;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn12;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn13;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn14;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn15;
        private System.Windows.Forms.DataGridViewButtonColumn dataGridViewButtonColumn3;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn16;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn17;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn18;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn19;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn20;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn21;
        private System.Windows.Forms.Button btnAbrirChamado;
        private System.Windows.Forms.Button button1;
    }
}