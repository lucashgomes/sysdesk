﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CRM.Fila_de_Chamados;

namespace CRM
{
    public partial class frmMenu_Principal : Form
    {
        public frmMenu_Principal()
        {
            InitializeComponent();
        }

        private void btnNovo_Usuario_Click(object sender, EventArgs e)
        {

        }

        private void btnAbrirChamado_Click(object sender, EventArgs e)
        {
            Abertura_de_Chamado.AberturaChamado abrirChamado = new Abertura_de_Chamado.AberturaChamado();
            abrirChamado.ShowDialog();
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmFilaDeChamados filaChamados = new frmFilaDeChamados();
            filaChamados.Show();
        }
    }
}
